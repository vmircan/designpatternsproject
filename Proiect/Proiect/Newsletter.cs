﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect
{
    public class Newsletter : IObservable<Announcement>
    {
        private List<IObserver<Announcement>> subscribers = new List<IObserver<Announcement>>();

        public IDisposable Subscribe(IObserver<Announcement> observer)
        {
            if (!subscribers.Contains(observer))
                subscribers.Add(observer);
            return new Unsubscriber { Subscribers = subscribers, Observer = observer };
        }

        public void PublishAnnouncement(Announcement announcement)
        {
            foreach (var subscriber in subscribers)
            {
                subscriber.OnNext(announcement);
            }
        }

        private class Unsubscriber : IDisposable
        {
            public List<IObserver<Announcement>> Subscribers
            {
                get; set;
            }
            public IObserver<Announcement> Observer
            {
                get; set;
            }

            public void Dispose()
            {
                if (Observer != null && Subscribers.Contains(Observer))
                    Subscribers.Remove(Observer);
            }
        }
    }
}
