﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect
{
    public class VipUserDecorator : UserDecorator
    {
        public VipUserDecorator(AbstractUser user) : base(user)
        {

        }

        public ComplaintResponse PostComplaint(Cinema cinema, Complaint complaint)
        {
            complaint.User = this.decoratedUser;
            return cinema.PostComplaint(complaint);
        }

        public override ISet<UserActionType> ValidActions
        {
            get
            {
                return new SortedSet<UserActionType> { UserActionType.BookMovie, UserActionType.PostComplaint, UserActionType.ViewMovie, UserActionType.SubcribeToNewsletter, UserActionType.UnsubscribeFromNewsletter, UserActionType.Logout };
            }
        }
    }
}
