﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect
{
    public class Client
    {
        private Cinema cinema;
        public Client(Cinema cinema)
        {
            this.cinema = cinema;   
        }

        public void Start()
        {
            DoLogin();
        }

        private ISet<AbstractUser> registeredUsers = new HashSet<AbstractUser> {
            new BasicUser("Vlad", "M"),
            new VipUserDecorator(new BasicUser("Irina", "P")),
            new AdminUserDecorator(new BasicUser("IrinaSefa", "$"))
        };

        private void DoLogin()
        {
            Console.WriteLine("Login:");
            AbstractUser user = null;
            bool success = false;
            while (!success)
            {
                Console.WriteLine("\nUsername:");
                var username = Console.ReadLine();
                Console.WriteLine("\nPassword:");
                var password = Console.ReadLine();
                user = GetUser(username, password);
                if (user != null)
                {
                    Console.WriteLine($"\nLogin Successful!\nLogged in as {user}\n");
                    success = true;
                }
                else
                {
                    Console.WriteLine("\nLogin Failed!\n");

                }
            }
            MainLoop(user);
        }

        private AbstractUser GetUser(string username, string password)
        {
            return registeredUsers.FirstOrDefault(user => user.Username == (username?.Trim() ?? "") && user.Password == (password?.Trim() ?? ""));
        }

        private void MainLoop(AbstractUser user)
        {
            var actionInvoker = new ActionInvoker(user, cinema);
            bool exited = false;
            while (!exited)
            {
                var loggedOut = false;
                if (!GetUserOption(actionInvoker))
                {
                    Console.WriteLine("\nYou were logged out\n");
                    loggedOut = true;
                }
                Console.WriteLine("\nKeep Going? (_/n)\n");
                var line = Console.ReadLine();
                if (line != null && line.Length > 0)
                {
                    if (line.ToLowerInvariant()[0] == 'n')
                    {
                        exited = true;
                    }
                }
                if (!exited && loggedOut)
                    DoLogin();
            }
        }

        private bool GetUserOption(ActionInvoker actionInvoker)
        {
            UserActionType userOption = (UserActionType)actionInvoker.User.ValidActions.GetEnumOrdinalChoice<UserActionType>();
            if (userOption == UserActionType.Logout)
                return false;
            actionInvoker.ProcessAction(userOption);
            return true;
        }

    }
}
