﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect
{
    public class Movie
    {
        public string Title
        {
            get; set;
        } = "";

        public string Description
        {
            get; set;
        } = "";

        public string Director
        {
            get;set;
        }

        public List<string> Cast
        {
            get; set;
        } = new List<string>();

        public double Rating
        {
            get;set;
        }
    }
}
