﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect
{
    public class BasicUser : AbstractUser
    {
        public BasicUser(string username, string password) : base(username, password)
        {
        }

        public override UserType UserType
        {
            get
            {
                return UserType.BasicUser;
            }
        }

        public override ISet<UserActionType> ValidActions
        {
            get
            {
                return new SortedSet<UserActionType> { UserActionType.BookMovie, UserActionType.ViewMovie, UserActionType.SubcribeToNewsletter, UserActionType.UnsubscribeFromNewsletter, UserActionType.Logout };
            }
        }

        public override void OnCompleted()
        {
            throw new NotImplementedException();
        }

        public override void OnError(Exception error)
        {
            throw new NotImplementedException();
        }

        public override void OnNext(Announcement announcement)
        {
            Console.WriteLine(Username + " " + announcement);
        }
    }
}
