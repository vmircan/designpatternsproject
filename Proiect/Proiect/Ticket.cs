﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect
{
    public enum TicketType
    {
        BasicTicket,
        VipTicket
    }

    public enum Extras
    {
        Popcorn,
        Juice,
        Nachos,
        Peanuts,
        Chips,
        Beer
    }

    public abstract class Ticket
    {
        public Screening Screening
        {
            get; set;
        }

        public Seat Seat
        {
            get; set;
        }

        public abstract ISet<Extras> AddedExtras
        {
            get;
        }

        public abstract string Name
        {
            get;
        }

        public abstract double Price
        {
            get;
        }

        public override string ToString()
        {
            return $"{Name} :\nPrice : {Price}$\nScreening : {Screening}\nSeat : {Seat}\n" + (AddedExtras.Count > 0 ? $"Added Extras :{AddedExtras.GetPrintableForm(",", "\n")}" : "");
        }
    }
}
