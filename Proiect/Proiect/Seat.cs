﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect
{
    public struct Seat
    {
        public int RowNumber;

        public int SeatNumber;

        public override string ToString()
        {
            return $"{(char)(RowNumber + 'A')}, {SeatNumber + 1}";
        }
    }
}
