﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect
{
    public struct Announcement
    {
        public string Message;

        public DateTime Time;

        public override string ToString()
        {
            return $"{Message} - {Time}";
        }
    }
}
