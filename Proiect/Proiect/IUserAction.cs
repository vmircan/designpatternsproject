﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect
{
    public enum UserActionType
    {
        BookMovie,
        PostComplaint,
        ViewMovie,
        SubcribeToNewsletter,
        UnsubscribeFromNewsletter,
        HireEmployee,
        FireEmployee,
        PostAnnouncement,
        Logout
    }

    public interface IUserAction
    {
        void DoAction();

        UserActionType ActionType
        {
            get; set;
        }

        AbstractUser User
        {
            get;
        }

        Cinema Cinema
        {
            get;
        }
    }
}
