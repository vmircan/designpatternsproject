﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Proiect
{
    public class ActionHandler
    {

        public void HandleAction(IUserAction action)
        {
            switch (action.ActionType)
            {
                case UserActionType.BookMovie:
                    ChooseMovie(action);
                    break;
                case UserActionType.PostComplaint:
                    PostComplaint(action);
                    break;
                case UserActionType.ViewMovie:
                    ViewMovie(action);
                    break;
                case UserActionType.HireEmployee:
                    HireEmployee(action);
                    break;
                case UserActionType.FireEmployee:
                    FireEmployee(action);
                    break;
                case UserActionType.SubcribeToNewsletter:
                    SubcribeToNewsletter(action);
                    break;
                case UserActionType.UnsubscribeFromNewsletter:
                    UnsubscribeFromNewsletter(action);
                    break;
                case UserActionType.PostAnnouncement:
                    PostAnnouncement(action);
                    break;
            }
        }

        private void PostAnnouncement(IUserAction action)
        {
            Console.WriteLine("Please enter a message to be sent out to subscribers");
            var line = Console.ReadLine();
            Console.WriteLine("\nAnnouncement posted\n");
            action.Cinema.PublishAnnouncement(new Announcement { Message = line, Time = DateTime.Now });
        }

        private void UnsubscribeFromNewsletter(IUserAction action)
        {
            if(action.User.UnsubscribeFromNewsletter())
                Console.WriteLine("\nYou will no longer receive announcements");
        }

        private void SubcribeToNewsletter(IUserAction action)
        {
            action.User.Subscribe(action.Cinema.Newsletter);
            Console.WriteLine("You subscribed to the newsletter");
        }

        private void ViewMovie(IUserAction action)
        {
            int choice;
            var groupedBookings = action.User.Bookings.GroupBy(it => it.Screening).Select(group => new { Screening = group.Key, Bookings = group.ToList() }).ToList();
            if(groupedBookings.Count == 0)
            {
                Console.WriteLine($"\nYou have no bookings\n");
                return;
            }
            while (true)
            {
                for (int i = 0; i < groupedBookings.Count; i++)
                {
                    Console.WriteLine($"Press {i + 1} for {groupedBookings[i].Bookings.FirstOrDefault().Screening.Movie.Title}");
                }
                var line = Console.ReadLine();
                if(line!=null && line.Length > 0)
                {
                    int.TryParse(line, out choice);
                    choice--;
                    if(choice >=0 && choice < groupedBookings.Count)
                    {
                        break;
                    }
                }
            }
            var chosenScreening = groupedBookings[choice].Bookings.FirstOrDefault().Screening;

            action.User.Bookings.RemoveAll(book => book.Screening == chosenScreening);

            Console.WriteLine($"You have watched {chosenScreening.Movie.Title}\n");
            action.Cinema.RemoveScreening(chosenScreening);
        }

        private void HireEmployee(IUserAction action)
        {
            Console.WriteLine("\nEmployee Name:");
            var line = Console.ReadLine();
            if(line!=null && line.Length > 0)
            {
                (action.User as AdminUserDecorator).HireEmployee(action.Cinema, new Employee(line));
            }
        }

        private void FireEmployee(IUserAction action)
        {
            var employees = action.Cinema.Employees;
            var madeValidChoice = false;
            int choice = 0;
            while (!madeValidChoice)
            {
                for (int i = 0; i < employees.Count; i++)
                {
                    Console.WriteLine($"Press {i + 1} to fire {employees[i].Name}");
                }
                var line = Console.ReadLine();
                if (line != null && line.Length > 0)
                {
                    if(int.TryParse(line, out choice))
                    {
                        if(choice>0 && choice <= employees.Count)
                        {
                            --choice;
                            madeValidChoice = true;
                            (action.User as AdminUserDecorator).FireEmployee(action.Cinema, employees[choice]);
                        }
                        else
                        {
                            Console.WriteLine("Pick from specified options\n");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Please specify a valid option\n");
                    }
                } 
                else
                {
                    Console.WriteLine("We'll be here until you enter something");
                }
            }
        }

        private void PostComplaint(IUserAction action)
        {
            Console.WriteLine("\nPlease share with us your complaint\n");
            ComplaintType complaintType = (ComplaintType)ComplaintType.ServiceComplaint.GetEnumOrdinalChoice<ComplaintType>();
            Complaint complaint = default(Complaint);
            complaint = new Complaint { User = action.User, Type = complaintType };
            Console.WriteLine("\nAdd a complaint message\n");
            var line = Console.ReadLine();
            complaint.ComplaintMessage = line;
            var complaintResponse = (action.User as VipUserDecorator).PostComplaint(action.Cinema, complaint);
            Console.WriteLine(complaintResponse);
        }

        private void ChooseMovie(IUserAction action)
        {
            var screenings = action.Cinema.GetFutureScreenings();
            var madeValidChoice = false;
            while (!madeValidChoice)
            {
                for (int i = 0; i < screenings.Count; i++)
                {
                    Console.WriteLine($"Press {i + 1} for:\n{screenings[i]}");
                }
                var line = Console.ReadLine();
                if (line != null && line.Length > 0)
                {
                    int choice;
                    if (!int.TryParse(line, out choice))
                    {
                        Console.WriteLine("Please specify a valid option\n");
                    }
                    else if (choice > 0 && choice <= screenings.Count)
                    {
                        --choice;
                        madeValidChoice = true;
                        SelectSeats(action.User, action.Cinema, screenings[choice]);
                    }
                    else
                    {
                        Console.WriteLine("Please specify a valid option\n");
                    }

                }
            }
        }

        private void SelectSeats(AbstractUser user, Cinema cinema, Screening screening)
        {
            Console.WriteLine(screening);
            Console.WriteLine(screening.GetSeatMap());
            var regex = new Regex(@"([A-Z]),\s*(\d+)");

            bool madeValidChoice = false;

            var ticketList = new List<Ticket>();

            while (!madeValidChoice)
            {
                var line = Console.ReadLine();
                if (line != null && line.Length > 0)
                {
                    var matches = regex.Matches(line);
                    if (matches.Count == 0)
                    {
                        Console.WriteLine("Please enter in a A,00 format like C,23 B,11...");
                    }
                    foreach (Match match in matches)
                    {
                        Console.WriteLine(match.Groups["Identifier"].Value);
                        Group row = match.Groups[1];
                        int rowNumber = row.Value[0] - (int)'A';
                        int seatNumber;
                        if (int.TryParse(match.Groups[2].Value, out seatNumber))
                        {
                            if (rowNumber < screening.Room.NumberOfRows && seatNumber <= screening.Room.NumberOfSeats)
                            {
                                var seat = new Seat { RowNumber = rowNumber, SeatNumber = --seatNumber };
                                if (!screening.IsSeatBooked(seat))
                                {
                                    madeValidChoice = true;
                                    ticketList.Add(MakeBooking(screening, seat));
                                }
                                else
                                {
                                    Console.WriteLine("Seat already booked");
                                }
                            }
                            else
                            {
                                Console.WriteLine("Please select from the seats available");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Please make a valid choice");
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Please enter in a A,00 format like C,23 B,11...");
                }
            }

            var booking = new Booking(user, ticketList);
            user.Bookings.Add(booking);
            cinema.AddBooking(booking);
            Console.WriteLine(screening.GetSeatMap());
        }

        private Ticket MakeBooking(Screening screening, Seat seat)
        {
            var ticketType = (TicketType)TicketType.BasicTicket.GetEnumOrdinalChoice<TicketType>();

            Ticket ticket = TicketFactory.Instance.GetTicket(ticketType);

            ticket.Seat = seat;
            ticket.Screening = screening;

            Console.WriteLine(ticket);

            return ticket;
        }
    }
}
