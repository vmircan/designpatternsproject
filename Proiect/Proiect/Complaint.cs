﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect
{
    public enum ComplaintType
    {
        ServiceComplaint,
        StaffComplaint
    }

    public enum ComplaintStatus
    {
        Solved,
        Pending,
        Rejected
    }

    public struct Complaint {
        public string ComplaintMessage;
        public ComplaintType Type;
        public AbstractUser User;
    }

    public class ComplaintResponse
    {
        public string Response
        {
            get;set;
        }

        public Complaint Complaint {
            get;set;
        }

        public ComplaintStatus Status
        {
            get;set;
        }

        public override string ToString()
        {
            return $"Response : {Response}\nStatus : {Status}";
        }
    }
}
