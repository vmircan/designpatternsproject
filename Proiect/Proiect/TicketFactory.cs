﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect
{
    public class TicketFactory
    {
        private static TicketFactory instance = null;

        public static TicketFactory Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new TicketFactory();
                }
                return instance;
            }
        }

        public Ticket GetTicket(TicketType ticketType)
        {
            switch (ticketType)
            {
                case TicketType.VipTicket:
                    return new VipTicket();
                default:
                    return new BasicTicket();
            }
        }

        private TicketFactory()
        {

        }
    }
}