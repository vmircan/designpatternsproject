﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect
{
    public static class Extensions
    {
        static Random random = new Random(234);

        public static T GetRandomElement<T>(this List<T> list)
        {
            return list[random.Next(0, list.Count)];
        }

        public static string GetPrintableForm<T>(this IEnumerable<T> list, string separator = " ", string terminator = "")
        {
            var sb = new StringBuilder();
            list.Aggregate(sb, (stringBuilder, item) => sb.Append(item.ToString() + separator));
            sb.Append(terminator);
            return sb.ToString();
        }

        public static int GetEnumOrdinalChoice<E>(this System.Enum e)
        {
            bool madeValidChoice = false;
            int option = 0;
            while (!madeValidChoice)
            {
                foreach (E value in Enum.GetValues(typeof(E)))
                {
                    Console.WriteLine($"Press {Convert.ToInt32(value) + 1} to {value.ToString()}");
                }
                var line = Console.ReadLine();
                if (line != null && line.Length > 0)
                {
                    int userOptionOrdinal;
                    if (int.TryParse(line, out userOptionOrdinal))
                    {
                        userOptionOrdinal--;
                        if (Enum.IsDefined(typeof(E), userOptionOrdinal))
                        {
                            option = userOptionOrdinal;
                            madeValidChoice = true;
                        }
                        else
                        {
                            Console.WriteLine("Please provide an input in the give range");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Please provide a valid input");
                    }
                }
            }

            return option;
        }


        public static int GetEnumOrdinalChoice<E>(this IEnumerable<E> values)
        {
            bool madeValidChoice = false;
            int option = 0;
            while (!madeValidChoice)
            {
                foreach (E value in values)
                {
                    Console.WriteLine($"Press {Convert.ToInt32(value) + 1} to {value.ToString()}");
                }
                var line = Console.ReadLine();
                if (line != null && line.Length > 0)
                {
                    int userOptionOrdinal;
                    if (int.TryParse(line, out userOptionOrdinal))
                    {
                        userOptionOrdinal--;
                        if (Enum.IsDefined(typeof(E), userOptionOrdinal))
                        {
                            option = userOptionOrdinal;
                            madeValidChoice = true;
                        }
                        else
                        {
                            Console.WriteLine("Please provide an input in the give range");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Please provide a valid input");
                    }
                }
            }

            return option;
        }

    }
}
