﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect
{

    public enum UserType
    {
        BasicUser,
        VipUser
    }
    public abstract class AbstractUser : IObserver<Announcement>
    {

        public AbstractUser(string username, string password)
        {
            this.Username = username;
            this.Password = password;
        }
        public abstract UserType UserType
        {
            get;
        }

        public abstract ISet<UserActionType> ValidActions
        {
            get;
        }

        private IDisposable unsubscriber = null;

        public void Subscribe(IObservable<Announcement> observable)
        {
            this.unsubscriber = observable?.Subscribe(this);
        }

        public bool UnsubscribeFromNewsletter() {
            if(unsubscriber == null)
            {
                Console.WriteLine("\nYou need to subscribe first\n");
                return false;
            }
            else
            {
                unsubscriber.Dispose();
                return true;
            }
        }

        public abstract void OnCompleted();

        public abstract void OnError(Exception error);

        public abstract void OnNext(Announcement announcement);

        public string Username
        {
            get; private set;
        }

        public string Password
        {
            get; private set;
        }

        public List<Booking> Bookings
        {
            get; private set;
        } = new List<Booking>();

        public override bool Equals(object obj)
        {
            if (!(obj is BasicUser))
                return false;
            var userCast = (obj as BasicUser);
            return this.Username == userCast.Username && this.Password == userCast.Password;
        }

        public override int GetHashCode()
        {
            var hashCode = 945308968;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Username);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Password);
            return hashCode;
        }

        public override string ToString()
        {
            return $"{Username}";
        }
    }
}
