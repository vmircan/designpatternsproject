﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect
{
    public class UserAction : IUserAction
    {

        public UserAction(AbstractUser user, Cinema cinema)
        {
            this.User = user;
            this.Cinema = cinema;
        }

        private ActionHandler actionHandler = new ActionHandler();

        public UserActionType ActionType {
            get;set;
        }

        public AbstractUser User
        {
            get; private set;
        }

        public Cinema Cinema  
        {
            get; private set;
        }

        public void DoAction()
        {
            actionHandler.HandleAction(this);   
        }
    }
}
