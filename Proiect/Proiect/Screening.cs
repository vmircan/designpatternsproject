﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect
{
    public class Screening : IComparable<Screening>
    {
        public Screening(CinemaRoom cinemaRoom, Movie movie, DateTime time, double price)
        { 
            this.Room = cinemaRoom;
            this.Movie = movie;
            this.Time = time;
            this.Price = price;
            bookedSeatsMap = new bool[Room.NumberOfRows, Room.NumberOfSeats];
            availableSeats = bookedSeatsMap.Length;
        }

        private int availableSeats;

        public int AvailableSeats
        {
            get
            {
                return availableSeats;
            }
        }

        public CinemaRoom Room
        {
            get; private set;
        }

        public DateTime Time
        {
            get; private set;
        }

        public Movie Movie
        {
            get;private set;
        }

        public double Price
        {
            get;private set;
        }

        private bool[,] bookedSeatsMap;

        public string GetSeatMap()
        {
            var sb = new StringBuilder();

            sb.Append("  ");
            Enumerable.Range(0, Room.NumberOfSeats).Aggregate(sb, (stringBuilder, number) => stringBuilder.Append(String.Format("{0,2} ", number+1)));
            sb.Append('\n');

            for (int i = 0; i < Room.NumberOfRows; i++)
            {
                char rowLetter = (char)(('A' + i));
                sb.Append(rowLetter + "  ");

                for (int j = 0; j < Room.NumberOfSeats; j++)
                {
                    sb.Append(bookedSeatsMap[i, j] ? 'X' : '_');
                    sb.Append("  ");
                }
                sb.Append('\n');
            }
            return sb.ToString();
        }

        public bool IsSeatBooked(Seat seat)
        {
            return (bookedSeatsMap[seat.RowNumber, seat.SeatNumber]);
        }

        public void BookSeat(Seat seat)
        {
            if(seat.RowNumber < Room.NumberOfRows && seat.SeatNumber < Room.NumberOfSeats)
            {
                bookedSeatsMap[seat.RowNumber, seat.SeatNumber] = true;
                --availableSeats;
            }
        }

        public int CompareTo(Screening other)
        {
            return this.Time.CompareTo(other.Time);
        }

        public override string ToString()
        {
            return $"{Time} ->\n{Movie.Title} ({Movie.Rating}) - Theater{Room.RoomNumber} ({AvailableSeats} / {Room.Capacity})\n{Movie.Description}\nDirector : {Movie.Director}\nCast : {Movie.Cast.GetPrintableForm(", ", "\n")}";
        }
    }
}
