﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect
{
    public class CinemaRoom : IComparable<CinemaRoom>
    {
        public CinemaRoom(int roomNumber, int numberOfRows, int numberOfColumns)
        {
            this.RoomNumber = roomNumber;
            this.NumberOfRows = numberOfRows;
            this.NumberOfSeats = numberOfColumns;
        }

        public int RoomNumber
        {
            get; private set;
        }

        public int Capacity
        {
            get
            {
                return NumberOfRows * NumberOfSeats;
            }
        }

        public int NumberOfRows
        {
            get; set;
        }

        public int NumberOfSeats
        {
            get; set;
        }

        public int CompareTo(CinemaRoom other)
        {
            return this.RoomNumber.CompareTo(other.RoomNumber);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is CinemaRoom))
                return false;
            var roomCast = (obj as CinemaRoom);
            return this.RoomNumber == roomCast.RoomNumber && this.Capacity == roomCast.Capacity;
        }
    }
}
