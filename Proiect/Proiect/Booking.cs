﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect
{
    public class Booking
    {
        public Booking(AbstractUser holder, IEnumerable<Ticket> tickets)
        {
            this.Holder = holder;
            this.Tickets.AddRange(tickets);
            this.Time = DateTime.Now;
            if(tickets.Count() > 0)
                this.Screening = tickets.First().Screening;
        }

        public Booking(AbstractUser holder, Ticket ticket)
        {
            this.Holder = holder;
            this.Tickets.Add(ticket);
            this.Time = DateTime.Now;
        }

        public AbstractUser Holder
        {
            get;private set;
        }

        public List<Ticket> Tickets
        {
            get; private set;
        } = new List<Ticket>();

        public DateTime Time
        {
            get;private set;
        }

        public Screening Screening
        {
            get; private set;
        }
    }
}
