﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect
{
    public class Manager : Employee
    {
        public Manager(string name) : base(name)
        {

        }

        public override ComplaintResponse AddressComplaint(Complaint complaint)
        {
            if (ApplicableComplaints.Contains(complaint.Type))
            {
                return new ComplaintResponse { Response = "Complaint accepted by manager", Complaint = complaint, Status = ComplaintStatus.Solved };
            }
            return new ComplaintResponse { Response = "Complaint unacceptable", Complaint = complaint, Status = ComplaintStatus.Rejected };
        }

        public override ISet<ComplaintType> ApplicableComplaints
        {
            get;
        } = new SortedSet<ComplaintType>() { ComplaintType.StaffComplaint };
    }
}
