﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect
{
    public class Cinema
    {
        public Cinema(Manager manager)
        {
            this.manager = manager;
            Employees = new List<Employee>();
        }

        private ISet<Screening> screenings = new SortedSet<Screening>();

        public void HireEmployee(Employee employee)
        {
            employee.Supervisor = manager;
            Employees.Add(employee);
            Console.WriteLine($"\n{employee.Name} was hired");
            Console.WriteLine("\nCurrent List of Employees:\n\n" + Employees.GetPrintableForm("\n", "\n"));
        }
        public void FireEmployee(Employee employee)
        {
            Employees.Remove(employee);
            Console.WriteLine($"\n{employee.Name} was fired");
            Console.WriteLine("\nCurrent List of Employees:\n\n" + Employees.GetPrintableForm("\n", "\n"));
        }
        public void HireEmployees(IEnumerable<Employee> employees)
        {
            foreach(var employee in employees)
            {
                employee.Supervisor = manager;
            }
            this.Employees.AddRange(employees);
        }

        public void AddScreening(Screening screening)
        {
            screenings.Add(screening);
        }

        public List<Screening> GetFutureScreenings()
        {
            return screenings.Where(screening => screening.Time > DateTime.Now).ToList();
        }

        public void AddScreenings(IEnumerable<Screening> screenings)
        {
            this.screenings.UnionWith(screenings);
        }

        public string GetUpcomingSchedule()
        {
            return GetFutureScreenings().GetPrintableForm("\n");
        }

        public ComplaintResponse PostComplaint(Complaint complaint)
        {
            if (Employees.Count == 0)
                return manager.AddressComplaint(complaint);
            return Employees.GetRandomElement().AddressComplaint(complaint);
        }

        private Dictionary<Screening, List<Booking>> screeningBookings = new Dictionary<Screening, List<Booking>>();

        public List<Employee> Employees
        {
            get; private set;
        }

        private Employee manager;

        public void AddBooking(Booking booking)
        {
            var screening = booking.Tickets.First().Screening;
            foreach(var ticket in booking.Tickets)
            {
                screening.BookSeat(ticket.Seat);
            }
            if (screeningBookings.ContainsKey(screening))
                screeningBookings[screening].Add(booking);
            else
                screeningBookings[screening] = new List<Booking>() { booking };
        }

        public void RemoveScreening(Screening screening)
        {
            if(screenings.Contains(screening))
                screenings.Remove(screening);
            if(screeningBookings.ContainsKey(screening))
                screeningBookings.Remove(screening);
        }

        public void RemoveBooking(Booking booking)
        {
            screeningBookings[booking.Screening].RemoveAll(book => book.Holder == booking.Holder);
        }

        public void AddRoom(CinemaRoom room)
        {
            Rooms.Add(room);
            Rooms.Sort();
        }

        public void AddRooms(IEnumerable<CinemaRoom> rooms)
        {
            Rooms.AddRange(rooms);
            Rooms.Sort();
        }

        public void PublishAnnouncement(Announcement announcement)
        {
            Newsletter.PublishAnnouncement(announcement);
        }

        public List<CinemaRoom> Rooms
        {
            get; set;
        } = new List<CinemaRoom>();

        public Newsletter Newsletter
        {
            get; private set;
        } = new Newsletter();

        public static Cinema GetDefaultCinemaInstance()
        {
            var cinema = new Cinema(new Manager("Clark"));
            cinema.AddRooms(new List<CinemaRoom>
            {
                new CinemaRoom(1, 20, 30),
                new CinemaRoom(2, 10, 15),
                new CinemaRoom(3, 15, 20),
                new CinemaRoom(4, 25, 30),
                new CinemaRoom(5, 10, 20),
            });
            cinema.HireEmployees(new List<Employee>
            {
                new Employee("Grace"),
                new Employee("Bob"),
            });
            cinema.AddScreenings(
                new List<Screening>()
                {
                    new Screening(cinema.Rooms.GetRandomElement(), movies.GetRandomElement(), DateTime.Now.AddDays(2).AddHours(1), 12.3),
                    new Screening(cinema.Rooms.GetRandomElement(), movies.GetRandomElement(), DateTime.Now.AddDays(3).AddHours(7), 13.3),
                    new Screening(cinema.Rooms.GetRandomElement(), movies.GetRandomElement(), DateTime.Now.AddDays(4).AddHours(2), 14.3),
                    new Screening(cinema.Rooms.GetRandomElement(), movies.GetRandomElement(), DateTime.Now.AddDays(5).AddHours(12), 11.3),
                });
            return cinema;
        }

        static List<Movie> movies = new List<Movie>()
        {
            new Movie{ Title = "Godfather", Director = "Francisc Copola", Description = "The best mafia movie", Rating = 9.2, Cast = new List<string>()
            {
                    "Al Pacino", "Marlon Brando", "Talia Shire"
                }
            },
            new Movie{ Title = "Schindler's List", Director = "Steven Spielberg", Description = "A rude awakening to the realities of the holocaust", Rating = 8.9, Cast = new List<string>()
            {
                    "Liam Neeson", "Ben Kingsley", "Ralph Fiennes"
                }
            },
            new Movie{ Title = "Saving Private Ryan", Director = "Steven Spielberg", Description = "A realistic depiction of D - Day and of war in general", Rating = 8.8, Cast = new List<string>()
            {
                    "Tom Hanks", "Matt Damon", "Vin Diesel"
                }
            },
            new Movie{ Title = "LOTR : The Fellowship Of The Ring", Director = "Peter Jackson", Description = "Embark on a 10 hour+ journey to Middle Earth", Rating = 8.9, Cast = new List<string>()
            {
                    "Eliajah Wood", "Sir Ian Mckellen", "Viggo Mortensen", "Orlando Bloom", "Hugo Weaving", "Cate Blanchet", "Cristopher Lee"
                }
            },
            new Movie{ Title = "LOTR : The Two Towers", Director = "Peter Jackson", Description = "The low point for the characters fleshes out their relationships and motives", Rating = 8.8, Cast = new List<string>()
            {
                    "Eliajah Wood", "Sir Ian Mckellen", "Viggo Mortensen", "Orlando Bloom", "Hugo Weaving", "Cate Blanchet", "Cristopher Lee"
                }
            },
            new Movie{ Title = "LOTR : The Return Of The King", Director = "Peter Jackson", Description = "The Epic conclusion to this perfectly crafted story", Rating = 8.9, Cast = new List<string>()
            {
                    "Eliajah Wood", "Sir Ian Mckellen", "Viggo Mortensen", "Orlando Bloom", "Hugo Weaving", "Cate Blanchet", "Cristopher Lee"
                }
            }
        };

    }
}
