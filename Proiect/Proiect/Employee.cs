﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect
{
    public class Employee
    {

        public Employee(string name)
        {
            this.Name = name;
        }

        public virtual ComplaintResponse AddressComplaint(Complaint complaint)
        {
            if (ApplicableComplaints.Contains(complaint.Type))
            {
                return new ComplaintResponse { Response = "Complaint accepted by employee", Complaint = complaint, Status = ComplaintStatus.Solved };
            }
            return Supervisor.AddressComplaint(complaint);
        }

        public Employee Supervisor
        {
            get;set;
        }

        public virtual ISet<ComplaintType> ApplicableComplaints
        {
            get;
        } = new SortedSet<ComplaintType>() { ComplaintType.ServiceComplaint };

        public string Name
        {
            get;private set;
        }

        public override string ToString()
        {
            return $"Employee {Name}" + (Supervisor != null ? $" - Supervisor {Supervisor.Name}" : "");
        }
    }
}
