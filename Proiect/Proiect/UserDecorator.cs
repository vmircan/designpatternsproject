﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect
{
    public abstract class UserDecorator : AbstractUser
    {
        protected AbstractUser decoratedUser;

        public override UserType UserType
        {
            get
            {
                return decoratedUser.UserType;
            }
        }

        public UserDecorator(AbstractUser user) : base(user.Username, user.Password)
        {
            this.decoratedUser = user;
        }

        public override void OnCompleted()
        {
            decoratedUser.OnCompleted();
        }

        public override void OnError(Exception error)
        {
            decoratedUser.OnError(error);
        }

        public override void OnNext(Announcement announcement)
        {
            decoratedUser.OnNext(announcement);
        }
    }
}
