﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect
{
    public class ActionInvoker
    {

        private IUserAction userAction;

        public ActionInvoker(AbstractUser user, Cinema cinema)
        {
            userAction = new UserAction(user, cinema);
            this.User = user;
            this.Cinema = cinema;
        }

        public void ProcessAction(UserActionType actionType)
        {
            userAction.ActionType = actionType;
            userAction.DoAction();
        }

        public AbstractUser User
        {
            get;private set;
        }

        public Cinema Cinema
        {
            get; private set;
        }
    }
}
