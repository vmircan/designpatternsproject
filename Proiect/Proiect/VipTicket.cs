﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect
{
    class VipTicket : Ticket
    {
        public override ISet<Extras> AddedExtras
        {
            get;
        } = new SortedSet<Extras>
        {
            Extras.Beer, Extras.Chips
        };

        public override string Name
        {
            get
            {
                return "Vip Ticket";
            }
        }

        public override double Price
        {
            get
            {
                return 25.0;
            }
        }
    }
}
