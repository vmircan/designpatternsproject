﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect
{
    class BasicTicket : Ticket
    {
        public override ISet<Extras> AddedExtras
        {
            get;
        } = new SortedSet<Extras>();

        public override string Name
        {
            get
            {
                return "Basic Ticket";
            }
        }

        public override double Price
        {
            get
            {
                return 15.0;
            }
        }
    }
}
