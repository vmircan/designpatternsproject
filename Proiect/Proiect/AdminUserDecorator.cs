﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect
{
    class AdminUserDecorator : UserDecorator
    {
        public AdminUserDecorator(AbstractUser user) : base(user)
        {

        }

        public override ISet<UserActionType> ValidActions
        {
            get
            {
                return new SortedSet<UserActionType> { UserActionType.HireEmployee, UserActionType.Logout, UserActionType.FireEmployee, UserActionType.PostAnnouncement };
            }
        }

        public void HireEmployee(Cinema cinema, Employee employee)
        {
            cinema.HireEmployee(employee);
        }

        public void FireEmployee(Cinema cinema, Employee employee)
        {
            cinema.FireEmployee(employee);
        }
    }
}
